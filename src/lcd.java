public class lcd {

    //atribut
    private String status;
    private int volume;
    private int brightness;
    private String cable;
    private int Cable;

    //method
    public String turnOff(){
        return this.status = "Mati";
    }
    public String turnOn(){
        return this.status = "Hidup";
    }
    public String freeze(){
        return this.status = "Freeze";
    }
    public int volumeUp(){
        return this.volume++;
    }
    public int volumeDown(){
        return this.volume--;
    }
    public int setVolume(int volume){
        return this.volume=volume;
    }
    public int brightnessUp(){
        return this.brightness++;
    }
    public int brightnessDown(){
        return this.brightness--;
    }
    public int setBrightness(int brightness){
        return this.brightness=brightness;
    }
    public void cableUp(){
     this.Cable++;
        switch (this.Cable){
            case 1 : this.cable ="VGA";
                break;
            case 2 : this.cable ="DVI";
                break;
            case 3 : this.cable ="HDMI";
                break;
            case 4 : this.cable ="Display Port";
                break;
        }
    }
    public void cableDown(){
        this.Cable--;
        switch (this.Cable){
            case 1 : this.cable ="VGA";
                break;
            case 2 : this.cable ="DVI";
                break;
            case 3 : this.cable = "HDMI";
                break;
            case 4 : this.cable = "Display Port";
                break;
        }
    }
    public void setCable(int number){
        this.Cable = number;
        switch (this.Cable){
            case 1 : this.cable ="VGA";
            break;
            case 2 : this.cable ="DVI";
            break;
            case 3 : this.cable = "HDMI";
            break;
            case 4 : this.cable = "Display Port";
            break;
        }
    }
    public void displayMessage(){
        System.out.println("Status LCD             = " + this.status);
        System.out.println("Volume LCD             = " + this.volume);
        System.out.println("Brightness LCD         = " + this.brightness);
        System.out.println("Kabel yang digunakan   = " + this.cable);
    }

}